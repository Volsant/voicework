﻿// Voice.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Voice!\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};

class Sheep : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Mee!\n";
	}
};

int main()
{

	const int size = 4;
	Animal* f[size] = { new Animal, new Dog, new Cat, new Sheep };

	for (Animal* a : f)
		a->Voice();


	delete f;
}

	
